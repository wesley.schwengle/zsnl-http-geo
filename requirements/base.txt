minty~=3.0,>=3.0.4
minty-pyramid~=2.1

minty_infra_amqp~=2.0
minty_infra_sqlalchemy~=3.0
python-json-logger

##  Project specific requirements
git+https://gitlab.com/xxllnc/zaakgericht/zaken/libraries/zsnl-pyramid.git@v2.0.1

