# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

"""
File contains custom `view` for Entity type `GeoFeature`
"""
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class GeoFeatureViews(JSONAPIEntityView):
    """
    Class represents a custom view for Entity `GeoFeature`
    """

    view_mapper = {
        "POST": {
            "create_geo_feature": {  # Must be EQ. MyRepo.Function()
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.geo",
                "run": "create_geo_feature",  # Query to execute
                "from": {
                    "json": {
                        "uuid": "uuid",
                        "geojson": "geojson",
                        "related_uuid": "related_uuid",
                    }
                },
                "command_primary": "uuid",
                "entity_type": "geo_feature",
            },
        },
        "GET": {
            "get_geo_features": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.geo",
                "run": "get_geo_features",
                "from": {"request_params": {"uuid": "uuid"}},
            }
        },
    }
